#! /bin/bash

##delft3d v4 config
## run domains separately
#deltares_hydro.exe config_flow2d3d_domain1.ini
#deltares_hydro.exe config_flow2d3d_domain2.ini
##run all with domain decomposition
#deltares_hydro.exe config_flow2d3d_dd.ini

#delft3d v6 config
## run domains separately
#d_hydro.exe config_d_hydro_domain1.xml
#d_hydro.exe config_d_hydro_domain2.xml
#run all with domain decomposition
#d_hydro.exe config_d_hydro_dd.xml
