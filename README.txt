These files are part of the OpenDA software. For more information see our website at
http://www.openda.org

Note: this is not an official version of the repository (which in in subversion https://svn.oss.deltares.nl/repos/openda/trunk) but a version in git which allows me to develop on various computers using git!

The master always remains a direct clone of the official subversion repository. From the log you can see to which version it corresponds.

