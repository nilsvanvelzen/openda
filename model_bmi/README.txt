This module contains code that can be used for connecting a model that adheres to the BMI interface to OpenDA using the BlackBoxStochModel. Unit tests for this code are also included.

To build this code, first run the public build.xml ANT script. Then run the model_bmi/build.xml ANT script, this creates the file model_bmi.jar in the model_bmi/bin folder.
